import java.util.Scanner;
public class TP1_2 {
    public static void main(String[] args) {
        // TODO: Kerjakan Soal 2 disini!
        Scanner masukin = new Scanner(System.in);
        System.out.println("Selamat datang di DDP2");
        System.out.print("Nama fakultas apa yang ingin anda ketahui?:");
        String singkatan = masukin.nextLine();
        switch(singkatan.toUpperCase()){
            case "FASILKOM":
                System.out.println("Fakultas Ilmu Komputer");
                break;
            case "FMIPA":
                System.out.println("Fakultas Matematika dan Ilmu Pengetahuan Alam");
                break;
            case "FEB":
                System.out.println("Fakultas Ekonomi dan Bisnis");
                break;
            case "FIB":
                System.out.println("Fakultas Ilmu Budaya");
                break;
            case "FH":
                System.out.println("Fakultas Hukum");
                break;
            case "FIA":
                System.out.println("Fakultas Ilmu Administrasi");
                break;
            case "FIK":
                System.out.println("Fakultas Ilmu Keperawatan");
                break;
            case "FK":
                System.out.println("Fakultas Kedokteran");
                break;
            case "FKG":
                System.out.println("Fakultas Kedokteran Gigi");
                break;
            case "FKM":
                System.out.println("Fakultas Kesehatan Masyarakat");
                break;
            case "FPSI":
                System.out.println("Fakultas Psikologi");
                break;
            case "FISIP":
                System.out.println("Fakultas Ilmu Sosial dan Ilmu Politik");
                break;
            default:
                System.out.println("Fakultas tidak ditemukan");
        }
    }
}