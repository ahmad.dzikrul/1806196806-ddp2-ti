public class Pakaian extends Barang {

    private char ukuran;
    private String warna;

    public Pakaian(char ukuran, String warna){
        this.ukuran = Character.toUpperCase(ukuran);
        this.warna = warna.toLowerCase();
    }

    public char getUkuran(){
        return this.ukuran;
    }

    public String getWarna(){
        return this.warna;
    }

    public Double getValue() {
        char[] Ukuran = {'L','M','S'};
        Double[] Harga = {40.0,35.0,30.0};
        Double harga = 0.0;
        for(int i = 0; i<Ukuran.length; i++){
            if(this.ukuran == Ukuran[i]){
                harga = Harga[i];
            }
        }
        return harga;
    }
    
    public String toString(){
        return this.ukuran+" "+this.warna.substring(0,1).toUpperCase()+this.warna.substring(1);
    }
}
