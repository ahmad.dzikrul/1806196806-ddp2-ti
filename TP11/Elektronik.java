public class Elektronik extends Barang {

    private String jenis;
    private String kondisi;

    public Elektronik(String jenis, String kondisi){
        this.jenis = jenis.toLowerCase();
        this.kondisi = kondisi.toLowerCase();
    }

    public String getJenis(){
        return this.jenis;
    }

    public String getKondisi(){
        return this.kondisi;
    }

    public Double getValue() {
        String[] Jenis = {"modem","laptop","hp"};
        String[] Kondisi = {"baru","baik","menengah","buruk"};
        Double[] Harga = {100.0,500.0,200.0};
        Double[] Nilai = {1.25,1.0,0.8,0.25};
        Double harga = 0.0;
        Double nilai = 0.0;
        for(int i = 0; i<Jenis.length; i++){
            if(this.jenis.equals(Jenis[i])){
                harga = Harga[i];
                break;
            }
        }
        for(int i = 0; i<Kondisi.length; i++){
            if(this.kondisi.equals(Kondisi[i])){
                nilai = Nilai[i];
                break;
            }
        }
        return harga*nilai;
    }

    public String toString(){
        return this.jenis.substring(0,1).toUpperCase()+this.jenis.substring(1)+" "+this.kondisi.substring(0,1).toUpperCase()+ this.kondisi.substring(1);
    }
    
}
