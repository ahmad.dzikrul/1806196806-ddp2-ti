import java.util.*;

public class Kardus<T extends Barang> {
    private ArrayList<T> listBarang;

    public Kardus(ArrayList<T> listBarang){
        this.listBarang = listBarang;
    }

    public String rekap(){
        int pakaian = 0;
        int elektronik =0;
        for(int i = 0; i<listBarang.size(); i++){
            try{
                ((Pakaian)listBarang.get(i)).toString();
                pakaian++;
            }catch(ClassCastException e){
                elektronik++;
            }
        }
        if(pakaian>0 && elektronik>0){
            return "Kardus Campuran: Terdapat " +elektronik+ " barang elektronik dan "+pakaian+" pakaian";
        }else if(pakaian>0){
            return "Kardus Pakaian: Terdapat " +elektronik+ " barang elektronik dan "+pakaian+" pakaian";
        }else{
            return "Kardus Elektronik: Terdapat " +elektronik+ " barang elektronik dan "+pakaian+" pakaian";
        }
    }

    public double getTotalValue(){
        Double harga=0.0;
        for(int i = 0; i<listBarang.size(); i++){
            try{
                harga+= ((Pakaian)listBarang.get(i)).getValue();
            }catch(ClassCastException e){
                harga+= ((Elektronik)listBarang.get(i)).getValue();
            }
        }
        return harga;
    }

    public void tambahBarang(T barang){
        listBarang.add(barang);
    }

    public ArrayList<T> getListBarang(){
        return this.listBarang;
    }

}
