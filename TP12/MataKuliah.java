import java.util.*;
import java.io.*;
public class MataKuliah {
    private String nama;
    private String kode;
    private Mahasiswa[] daftarMhs;
    private Dosen dosen;

    public MataKuliah(String nama, String kode){
        this.nama = nama;
        this.kode = kode;
    }

    public String klasifikasiMatKul(){
        String[] key = {"UIGE","UIST","CSGE","CSCM","CSIM","CSCE","CSIE"};
        String[] value = {
            "Mata Kuliah Wajib Universitas",
            "Mata Kuliah Wajib Rumpun Sains dan Teknologi",
            "Mata Kuliah Wajib Fakultas",
            "Mata Kuliah Wajib Program Studi Ilmu Komputer",
            "Mata Kuliah Wajib Program Studi Sistem Informasi",
            "Mata Kuliah Peminatan Program Studi Ilmu Komputer",
            "Mata Kuliah Peminatan Program Studi Sistem Informasi"
        };
        for(int i=0;i<key.length;i++){
            if(this.kode.substring(0,4).equals(key[i])){
                return value[i];
            }
        }
        return "Kode tidak ditemukan";
    }

    public void printDetail(){
        System.out.println("Peserta Mata Kuliah "+this.nama+" adalah");
        for(int i=0;i<this.daftarMhs.length;i++){
            System.out.println(this.daftarMhs[i].getNama());
        }
        System.out.println("Dosen pengampu mata kuliah ini adalah "+this.dosen.getNama());
    }

    public void assignDosen(Dosen dosen){
        this.dosen = dosen;
    }

    public Dosen getDosen(){
        return this.dosen;
    }

    public void tambahMhs(Mahasiswa mhs){
        List<Mahasiswa> data = new LinkedList<Mahasiswa>();
        try{
            List<Mahasiswa> input = new LinkedList<Mahasiswa>(Arrays.asList(this.daftarMhs));
            data = input;
        }catch(Exception e){
            Mahasiswa kul1 = new Mahasiswa("inisiasi","Test12345");
            Mahasiswa[] awal = {kul1};
            this.daftarMhs = awal;
            List<Mahasiswa> input = new LinkedList<Mahasiswa>(Arrays.asList(this.daftarMhs));
            data = input;
        }
        data.add(mhs);
        if(data.get(0).getNama().equals("inisiasi")){
            data.remove(0);
        }
        Mahasiswa[] arr = new Mahasiswa[data.size()]; 
        this.daftarMhs = data.toArray(arr);
    }

    public void writeFile(String namaFile) throws FileNotFoundException{
        java.io.File file = new java.io.File(namaFile);
        java.io.PrintWriter output = new java.io.PrintWriter(file);
        output.print(getKode()+",");
        output.println(getNama());
        for(int i =0;i<this.daftarMhs.length;i++){
            output.print(this.daftarMhs[i].getNpm()+",");
            output.println(this.daftarMhs[i].getNama());
        }
        output.close();
    }

    public void dropMhs(Mahasiswa mhs){
        try{
            List<Mahasiswa> data = new LinkedList<Mahasiswa>(Arrays.asList(this.daftarMhs));
            data.remove(mhs);
            Mahasiswa[] arr = new Mahasiswa[data.size()]; 
            this.daftarMhs = data.toArray(arr);
        }catch(Exception e){
            System.out.println("Daftar mahasiswa tidak ada");
        }

    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return this.nama;
    }

    public void setKode(String kode){
        this.kode = kode;
    }

    public String getKode(){
        return this.kode;
    }

    public void setDaftarMhs(Mahasiswa[] daftarMhs){
        this.daftarMhs = daftarMhs;
    }

    public Mahasiswa[] getDaftarMhs(){
        return this.daftarMhs;
    }

    public String toString(){
        return "";
    }
}
