import java.util.*;

public class Mahasiswa extends Manusia {
    private String npm;
    private MataKuliah[] daftarMatKul;

    public Mahasiswa(String nama, String npm){
        setNama(nama);
        this.npm = npm;
    }

    public void tambahMatKul(MataKuliah matkul){
        List<MataKuliah> data = new LinkedList<MataKuliah>();
        try{
            List<MataKuliah> input = new LinkedList<MataKuliah>(Arrays.asList(this.daftarMatKul));
            data = input;
        }catch(Exception e){
            MataKuliah kul1 = new MataKuliah("inisiasi","Test12345");
            MataKuliah[] awal = {kul1};
            this.daftarMatKul = awal;
            List<MataKuliah> input = new LinkedList<MataKuliah>(Arrays.asList(this.daftarMatKul));
            data = input;
        }
        data.add(matkul);
        if(data.get(0).getNama().equals("inisiasi")){
            data.remove(0);
        }
        MataKuliah[] arr = new MataKuliah[data.size()]; 
        this.daftarMatKul = data.toArray(arr);
    }

    public void printMatkul(){
        for(int i=0;i<this.daftarMatKul.length;i++){
            System.out.println(this.daftarMatKul[i].getNama());
        }
    }

    public void dropMatKul(MataKuliah matkul){
        try{
            List<MataKuliah> data = new LinkedList<MataKuliah>(Arrays.asList(this.daftarMatKul));
            data.remove(matkul);
            MataKuliah[] arr = new MataKuliah[data.size()]; 
            this.daftarMatKul = data.toArray(arr);
        }catch(Exception e){
            System.out.println("Daftar mata kuliah tidak ada");
        }
    }

    public void setDaftarMatKul(MataKuliah[] daftarMatkul){
        this.daftarMatKul = daftarMatkul;
    }

    public MataKuliah[] getDaftarMatkul(){
        return this.daftarMatKul;
    }

    public void setNpm(String npm){
        this.npm = npm;
    }

    public String getNpm(){
        return this.npm;
    }

    public String toString(){
        return "";
    }
}
