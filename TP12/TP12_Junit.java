import static org.junit.Assert.*;
import java.io.File;
import java.io.FileNotFoundException;
import org.junit.Test;
import java.util.Arrays;
import java.util.List;
 
public class TP12_Junit{
 
  @Test
  public void testKuliah01(){
      Mahasiswa mhs1 = new Mahasiswa("Burhan","129500004Y");
      Mahasiswa mhs2 = new Mahasiswa("Bubur","1299123456");
      MataKuliah kul1 = new MataKuliah("DDP2","CSGE601021");
      MataKuliah kul2 = new MataKuliah("uji","UI896647");
      MataKuliah kul3 = new MataKuliah("tesNgoding","CSGE123456");
      Dosen dosen = new Dosen("Pak Uji","789123456");
      Dosen dosen2 = new Dosen("Pak Tes","889123456");
 
      dosen.assignMatkul(kul1);
      kul1.assignDosen(dosen);
      kul1.tambahMhs(mhs1);
      kul1.tambahMhs(mhs2);
      mhs1.tambahMatKul(kul1);
      mhs1.tambahMatKul(kul2);

      Mahasiswa[] daftarMhs = kul1.getDaftarMhs();
      List<Mahasiswa> arr = Arrays.asList(daftarMhs);

      MataKuliah[] daftarMatkul = mhs1.getDaftarMatkul();
      List<MataKuliah> arr2 = Arrays.asList(daftarMatkul);

      kul2.setNama("ujis");
      kul2.setKode("UI12345");
      kul2.setDaftarMhs(daftarMhs);
      mhs2.setNama("Burcang");
      mhs2.setNpm("12345");
      mhs2.setDaftarMatKul(daftarMatkul);
      dosen2.setDaftarMatKul(daftarMatkul);
      try {
         dosen2.ReadData("dataTest.csv");
      } catch (Exception e) {
         System.out.println("File tidak ditemukan");
      }
      int jumlah = 1;

      Mahasiswa[] daftarMhs2 = kul2.getDaftarMhs();
      List<Mahasiswa> arr3 = Arrays.asList(daftarMhs2);

      MataKuliah[] daftarMatkul2 = mhs2.getDaftarMatkul();
      List<MataKuliah> arr4 = Arrays.asList(daftarMatkul2);

      MataKuliah[] daftarMatkulDosen = dosen2.getDaftarMatkul();
      List<MataKuliah> arr5 = Arrays.asList(daftarMatkulDosen);
      
      assertThrows(FileNotFoundException.class, () -> {
         dosen2.ReadData("salah.csv");
      });

      assertThrows(FileNotFoundException.class, () -> {
         kul1.writeFile("salah.csv");
      });

      assertEquals(dosen.getNama(), "Pak Uji");
      assertEquals(dosen.getNip(), "789123456");
      assertEquals(kul1.getDosen().getNama(), "Pak Uji");
      assertEquals(dosen.getDaftarMatkul()[0].getNama(), "DDP2");
      assertEquals(kul1.getNama(), "DDP2");
      assertEquals(kul1.getKode(), "CSGE601021");
      assertEquals(kul2.getNama(), "ujis");
      assertEquals(kul2.getKode(), "UI12345");
      assertEquals(mhs1.getNama(), "Burhan");
      assertEquals(mhs1.getNpm(), "129500004Y");
      assertEquals(mhs1.getNama(), "Burcang");
      assertEquals(mhs1.getNpm(), "12345");
      assertEquals(kul1.klasifikasiMatKul(), "Mata Kuliah Wajib Fakultas");
      assertTrue(arr.contains(mhs1));
      assertTrue(arr.contains(mhs2));
      assertTrue(arr2.contains(kul1));
      assertTrue(arr3.contains(mhs1));
      assertTrue(arr4.contains(kul1));
      assertTrue(arr5.contains(kul1));
      assertTrue(arr5.contains(kul3));
      mhs1.dropMatKul(kul2);
      kul1.dropMhs(mhs2);
      assertEquals(mhs1.getDaftarMatkul().length, jumlah);
      assertEquals(kul1.getDaftarMhs().length, jumlah);
   }
}
