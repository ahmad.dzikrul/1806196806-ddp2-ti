import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Mahasiswa mhs2 = new Mahasiswa("Bubur", "1299123456");
        Mahasiswa mhs1 = new Mahasiswa("Burhan", "129500004Y");
        MataKuliah kul1 = new MataKuliah("DDP2", "CSGE601021");
        MataKuliah kul2 = new MataKuliah("uji", "UI896647");
        Dosen dosen = new Dosen("Pak Uji", "789123456");
        mhs2.tambahMatKul(kul1);
        mhs2.tambahMatKul(kul2);
        mhs2.printMatkul();
        kul1.tambahMhs(mhs2);
        kul1.tambahMhs(mhs1);
        kul1.assignDosen(dosen);
        dosen.assignMatkul(kul1);
        dosen.setDaftarMatKul(mhs2.getDaftarMatkul());
        MataKuliah[] daftarMatkulDosen = dosen.getDaftarMatkul();
        List<MataKuliah> arr5 = Arrays.asList(daftarMatkulDosen);
        System.out.println(arr5.contains(kul1));
        System.out.println(dosen.getNama());
        System.out.println(kul1.getDosen().getNama());
        System.out.println(dosen.getDaftarMatkul()[0].getNama());
        System.out.println(kul1.klasifikasiMatKul());
        System.out.println(mhs2.getNama());
        kul1.printDetail();
        try {
            dosen.ReadData("dataTest.csv");
        } catch (Exception e) {
            System.out.println("File tidak ditemukan");
        }
        try {
            kul1.writeFile("hasilTest.csv");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
