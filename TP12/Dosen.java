import java.io.*;
import java.util.*;

public class Dosen extends Manusia {
    private String nip;
    private MataKuliah[] daftarMatKul;

    public Dosen(String nama, String nip) {
        setNama(nama);
        this.nip = nip;
    }

    public void ReadData(String nameFile) throws FileNotFoundException {
        java.io.File file = new java.io.File(nameFile);
        ArrayList<String[]> arrMahasiswa = new ArrayList<String[]>();
        Boolean ketemu = false;
        Scanner input = new Scanner(file);
        while (input.hasNext()) {
            String data = input.nextLine();
            String[] datas = data.split(",");
            arrMahasiswa.add(datas);            
        }
        MataKuliah matkul = new MataKuliah(arrMahasiswa.get(0)[1],arrMahasiswa.get(0)[0]);
        for(int i = 1; i<arrMahasiswa.size();i++){
            Mahasiswa mahasiswa = new Mahasiswa(arrMahasiswa.get(i)[1],arrMahasiswa.get(i)[0]);
            matkul.tambahMhs(mahasiswa);
        }
        for(int i = 0; i<this.daftarMatKul.length;i++){
            if(matkul.getNama().equals(daftarMatKul[i].getNama())){
                daftarMatKul[i] = matkul;
                ketemu = true;
                break;
            }
        }
        if(ketemu == false){
            assignMatkul(matkul);
        }
        input.close();
    }

    public void assignMatkul(MataKuliah matkul){
        List<MataKuliah> data = new LinkedList<MataKuliah>();
        try{
            List<MataKuliah> input = new LinkedList<MataKuliah>(Arrays.asList(this.daftarMatKul));
            data = input;
        }catch(Exception e){
            MataKuliah kul1 = new MataKuliah("inisiasi","Test12345");
            MataKuliah[] awal = {kul1};
            this.daftarMatKul = awal;
            List<MataKuliah> input = new LinkedList<MataKuliah>(Arrays.asList(this.daftarMatKul));
            data = input;
        }
        data.add(matkul);
        if(data.get(0).getNama().equals("inisiasi")){
            data.remove(0);
        }
        MataKuliah[] arr = new MataKuliah[data.size()]; 
        this.daftarMatKul = data.toArray(arr);
    }

    public void setNip(String nip){
        this.nip = nip;
    }

    public String getNip(){
        return this.nip;
    }

    public void setDaftarMatKul(MataKuliah[] daftarMatkul){
        this.daftarMatKul = daftarMatkul;
    }

    public MataKuliah[] getDaftarMatkul(){
        return this.daftarMatKul;
    }
}
