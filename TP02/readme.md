# 💻 Tugas Pemrograman 2 (Individu)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Nama**: Kak Burhan | **NPM**: 1906123456 | **Kelas**: J | **Kode Tutor**: RF

## Topik

Hi Class , I’m main!

## Dokumen Tugas

[https://drive.google.com/file/d/1koH8h11wRMqPj3T8BDfSG3unXO8AasYg/view?usp=sharing](https://drive.google.com/file/d/1koH8h11wRMqPj3T8BDfSG3unXO8AasYg/view?usp=sharing)

## *Checklist*

- [ ] Membaca dan memahami Soal Tugas Pemrograman 2 dengan seksama
- [ ] Menyelesaikan Soal 0
- [ ] Menyelesaikan Soal 1
- [ ] Menyelesaikan Soal 2
- [ ] Menyelesaikan Soal 3 dan memastikan output TP2_3.java sesuai permintaan
- [ ] Menyelesaikan Soal 4 dan memastikan output TP2_4.java sesuai permintaan
- [ ] Mengumpulkan hasil pengerjaan ke GitLab
- [ ] Melakukan peragaan Tugas Pemrograman ke Tutor