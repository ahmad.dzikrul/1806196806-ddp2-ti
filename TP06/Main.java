import java.util.Scanner;
public class Main {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Masukkan jumlah karyawan: ");
        int jumlahKaryawan = scan.nextInt();
        Karyawan[] daftarKaryawan = new Karyawan[jumlahKaryawan];
        
        for (int i = 0; i < jumlahKaryawan; i++) {
            System.out.print("Masukkan nama karyawan "+(i+1)+": ");
            String nama = scan.next();
            System.out.print("Masukkan umur karyawan "+(i+1)+": ");
            int umur = scan.nextInt();
            System.out.print("Masukkan lama bekerja karyawan "+(i+1)+": ");
            int lamaBekerja = scan.nextInt();
            Karyawan dataKaryawan = new Karyawan(nama, umur, lamaBekerja);
            daftarKaryawan[i] = dataKaryawan;
        }
        scan.close();
        System.out.printf("\nRata-rata gaji karyawan adalah %.2f%n", rerataGaji(daftarKaryawan));
        System.out.println("Karyawan dengan gaji tertinggi adalah " + gajiTertinggi(daftarKaryawan));
        System.out.println("Karyawan dengan gaji terendah adalah " + gajiTerendah(daftarKaryawan));
    }
    //TODO
    public static double rerataGaji(Karyawan[] daftarKaryawan) {
        double jumlahGaji = 0;
        for (Karyawan i : daftarKaryawan){
            jumlahGaji += i.getGaji();
        }
        double rataRata = jumlahGaji/daftarKaryawan.length;
        return rataRata;
    }
    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTerendah(Karyawan[] daftarKaryawan) {
        Karyawan terendah = daftarKaryawan[0];
        for (int i = 1; i < daftarKaryawan.length; i++){
            Karyawan cek = daftarKaryawan[i];
            if (cek.getGaji() < terendah.getGaji()){
                terendah = cek;
            }
        }
        return terendah.getNama();
    }

    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTertinggi(Karyawan[] daftarKaryawan) {
        Karyawan tertinggi = daftarKaryawan[0];
        for (int i = 1; i < daftarKaryawan.length; i++){
            Karyawan cek = daftarKaryawan[i];
            if (cek.getGaji() > tertinggi.getGaji()){
                tertinggi = cek;
            }
        }
        return tertinggi.getNama();
    }
}
