# 💻 Tugas Pemrograman 6 (Individu)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Nama**: Kak Burhan | **NPM**: 1906123456 | **Kelas**: J | **Kode Tutor**: KS

## Topik

I'm a Class. I model objects. What? 

## Dokumen Tugas

https://drive.google.com/file/d/1dlqkcR8f7zF4gfJvuQ_A-umT8ssnV6_R/view?usp=sharing

## *Checklist*

- [ ] Membaca dan memahami Soal Tugas Pemrograman 6 dengan sekesama
- [ ] Memahami class dan object serta penggunaannya
- [ ] Melengkapi class Karyawan sesuai dengan yang diinginkan pada soal 
- [ ] Melengkapi class Main sesuai dengan yang diinginkan pada soal
- [ ] Mengumpulkan hasil pengerjaan ke GitLab
- [ ] Melakukan peragaan Tugas Pemrograman ke Tutor