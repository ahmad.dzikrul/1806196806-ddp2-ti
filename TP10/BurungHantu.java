public class BurungHantu extends Hewan implements Makhluk{

	public BurungHantu(String nama, String spesies) {
        setNama(nama);
        setSpesies(spesies);
    }

	@Override
    public String bernafas() {
        // TODO Auto-generated method stub
        return getNama()+" bernafas dengan paru-paru.";
    }

    @Override
    public String bergerak() {
        // TODO Auto-generated method stub
        return getNama()+" bergerak dengan cara terbang.";
    }

    @Override
    public String bersuara() {
        // TODO Auto-generated method stub
        return "Hooooh hoooooooh.(Halo, saya "+getNama()+". Saya adalah burung hantu).";
    }
    
}
