public abstract class Manusia
{
    private String nama;
    private double uang;

    protected Manusia(){
    }
    protected Manusia(String nama, double uang){
        this.nama = nama;
        this.uang = uang;
    }
    public String getNama(){
        return nama;
    }
    public Double getUang(){
        return uang;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public void setUang(double uang){
        this.uang = uang;
    }
    public static void bandingkanUang(Manusia x, Manusia y) {
        if (x.getUang() < y.getUang()) {
            System.out.println(x.getNama() + " (" + x.getUang() + ") memiliki lebih sedikit uang dari " + y.getNama() + " (" + y.getUang() + ")");
        }
        else if (x.getUang() > y.getUang()){
            System.out.println(x.getNama() + " (" + x.getUang() + ") memiliki lebih banyak uang dari " + y.getNama() + " (" + y.getUang() + ")");
        }
        else {
            System.out.println("Jumlah uang kedua manusia sama, yaitu " + x.getUang());
        }
    }
    public abstract String bicara();
}