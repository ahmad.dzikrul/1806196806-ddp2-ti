public class Ikan extends Hewan implements Makhluk {
    private Boolean racun;

	public Ikan(String nama, String spesies, Boolean racun) {
        this.racun = racun;
        setNama(nama);
        setSpesies(spesies);
    }

	public String getRacun(){
        if (racun == true){
            return "beracun";
        }else{
            return "tidak beracun";
        }
    }

    @Override
    public String bernafas() {
        // TODO Auto-generated method stub
        return getNama()+" bernafas dengan insang.";
    }

    @Override
    public String bergerak() {
        // TODO Auto-generated method stub
        return getNama()+" bergerak dengan cara berenang.";
    }

    @Override
    public String bersuara() {
        // TODO Auto-generated method stub
        return "Blub blub blub blub. Blub. (Halo, saya "+getNama()+". Saya ikan yang "+ getRacun() +").";
    }

}
