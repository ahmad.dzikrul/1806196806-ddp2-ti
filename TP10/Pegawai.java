public class Pegawai extends Manusia implements Makhluk{
    private String levelKeahlian;

    public Pegawai(String nama, double uang, String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
        setNama(nama);
        setUang(uang);
    }

    public String getLevelKeahlian(){
        return levelKeahlian;
    }

    public void setLevelKeahlian(String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
    }

    @Override
    public String bicara() {
        // TODO Auto-generated method stub
        return "Halo, saya "+ getNama() +". Uang saya adalah "+getUang()+", dan level keahlian saya adalah "+getLevelKeahlian()+".";
    }

    @Override
    public String bernafas() {
        // TODO Auto-generated method stub
        return getNama()+" bernafas dengan paru-paru.";
    }

    @Override
    public String bergerak() {
        // TODO Auto-generated method stub
        return getNama()+" bergerak dengan cara berjalan.";
    }

    public String bekerja(){
        return getNama()+" bekerja di kedai VoidMain.";
    }
}
