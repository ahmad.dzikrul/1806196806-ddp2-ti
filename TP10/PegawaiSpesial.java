public class PegawaiSpesial extends Manusia implements Makhluk,bisaLibur{

    private String levelKeahlian;

    public PegawaiSpesial(String nama, double uang, String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
        setNama(nama);
        setUang(uang);
    }

    public String getLevelKeahlian(){
        return levelKeahlian;
    }

    public void setLevelKeahlian(String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
    }

    @Override
    public String bicara() {
        // TODO Auto-generated method stub
        return "Halo, saya "+ getNama() +". Uang saya adalah "+getUang()+", dan level keahlian saya adalah "+getLevelKeahlian()+". Saya memiliki privilege yaitu bisa libur.";
    }

    @Override
    public String bernafas() {
        // TODO Auto-generated method stub
        return getNama()+" bernafas dengan paru-paru.";
    }

    @Override
    public String bergerak() {
        // TODO Auto-generated method stub
        return getNama()+" bergerak dengan cara berjalan.";
    }

    public String bekerja(){
        return getNama()+" bekerja di kedai VoidMain.";
    }

    @Override
    public String libur() {
        // TODO Auto-generated method stub
        return getNama()+" sedang berlibur ke Akihabara.";
    }
    
}
