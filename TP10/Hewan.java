public abstract class Hewan {
    private String nama;
    private String spesies;

    protected Hewan(){
    }
    protected Hewan(String nama, String spesies){
        this.nama = nama;
        this.spesies = spesies;
    }

    public String getNama(){
        return nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getSpecies(){
        return spesies;
    }

    public void setSpesies(String spesies){
        this.spesies = spesies;
    }

    public abstract String bersuara();
}
