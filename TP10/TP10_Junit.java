import static org.junit.Assert.*; 
import org.junit.Test;

    public class TP10_Junit {
      Double uang = 100000.0;
      Manusia steve = new PegawaiSpesial("Steve", uang, "master");
      Manusia yoga = new Pegawai("Yoga", uang, "master");
      Manusia bujang = new Pelanggan("Bujang", uang);
      Hewan betta = new Ikan("Betta", "Betta Splendens", false);
      Hewan burhan = new BurungHantu("Burhan", "Bubo Scandiacus");
      Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);

      @Test
      public void testMethodExampleDanField(){
        assertEquals(bujang.getUang(),uang);
        assertEquals(yoga.getUang(),uang);
        assertEquals(steve.getUang(),uang); 
        assertEquals(bujang.getNama(),"Bujang");
        assertEquals(yoga.getNama(),"Yoga");
        assertEquals(steve.getNama(),"Steve");
        assertEquals(((PegawaiSpesial)yoga).getLevelKeahlian(),"master");
        assertEquals(((PegawaiSpesial)steve).getLevelKeahlian(),"master");
        assertEquals(((Makhluk)bujang).bergerak(),"Bujang bergerak dengan cara berjalan.");
        assertEquals(((Makhluk)yoga).bergerak(),"Yoga bergerak dengan cara berjalan.");
        assertEquals(((PegawaiSpesial)steve).bekerja(),"Steve bekerja di kedai VoidMain.");
        assertEquals(((Manusia)yoga).bicara(),"Halo, saya Yoga. Uang saya adalah 100000.0, dan level keahlian saya adalah master.");
        assertEquals(((Makhluk)yoga).bernafas(),"Yoga bernafas dengan paru-paru.");
        assertEquals(((Manusia)bujang).bicara(),"Halo, saya Bujang. Uang saya adalah 100000.0.");
        assertEquals(((Pelanggan)bujang).beli(),"Bujang membeli makanan dan minuman di kedai VoidMain.");
        assertEquals(((Hewan)betta).bersuara(),"Blub blub blub blub. Blub. (Halo, saya Betta. Saya ikan yang tidak beracun).");
        assertEquals(((Makhluk)betta).bernafas(),"Betta bernafas dengan insang.");
        assertEquals(((Makhluk)betta).bergerak(),"Betta bergerak dengan cara berenang.");
        assertEquals(((Makhluk)burhan).bernafas(),"Burhan bernafas dengan paru-paru.");
        assertEquals(((Makhluk)burhan).bergerak(),"Burhan bergerak dengan cara terbang.");
        assertEquals(((Hewan)betta).bersuara(),"Hooooh hoooooooh.(Halo, saya Burhan. Saya adalah burung hantu).");
        assertEquals(((Manusia)steve).bicara(),"Halo, saya Steve. Uang saya adalah 100000.0, dan level keahlian saya adalah master. Saya memiliki privilege yaitu bisa libur.");
        assertEquals(((Hewan)ikanTerbang).bersuara(),"Blub blub blub blub. Blub. Blub blub blub. (Halo, saya Ikan Terbang Biru. Saya ikan yang tidak beracun. Saya bisa terbang loh.).");
        assertEquals(((PegawaiSpesial)steve).libur(),"Steve sedang berlibur ke Akihabara.");
        assertEquals(((IkanSpesial)ikanTerbang).terbang(),"Fwooosssshhhhh! Plup.");
      }

      @Test
      public void testSubClass(){
      assertEquals(yoga instanceof Manusia, true);
      assertEquals(yoga instanceof Pegawai, true);
      assertEquals(bujang instanceof Manusia, true);
      assertEquals(bujang instanceof Pelanggan, true);
      assertEquals(steve instanceof Manusia, true);
      assertEquals(steve instanceof PegawaiSpesial, true);
      assertEquals(betta instanceof Hewan, true);
      assertEquals(betta instanceof Ikan, true);
      assertEquals(burhan instanceof Hewan, true);
      assertEquals(burhan instanceof BurungHantu, true);
      }

      @Test 
      public void testImplements(){
        assertEquals(burhan instanceof Makhluk, true);
        assertEquals(betta instanceof Makhluk, true);
        assertEquals(bujang instanceof Makhluk, true);
        assertEquals(yoga instanceof Makhluk, true);
        assertEquals(steve instanceof Makhluk, true);
        assertEquals(steve instanceof bisaLibur, true);
        assertEquals(ikanTerbang instanceof Makhluk, true);
        assertEquals(ikanTerbang instanceof bisaTerbang, true);
      }
}


