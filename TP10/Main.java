import java.util.Scanner;
import java.util.ArrayList;
public class Main{
    public static void main(String[] args){
        boolean booleanRacun = false;
        String keterangan ="";
        String Perintah = "";
        String status = "";
        String nama_objek = "";
        int indexObjek = -1;
        boolean cekMakhluk = false;
        boolean cekKeterangan = false;
        ArrayList<Object> objects = new ArrayList<>();
        Scanner inp = new Scanner(System.in);
        System.out.print("Ada berapa makhluk: ");
        int x = Integer.parseInt(inp.nextLine());

    
        for(int i=0; i<x; i++){ 
            while(cekMakhluk == false){
                System.out.print("Makhluk apa Anda: ");
                status = inp.nextLine().toLowerCase();
                if(status.equals("manusia")){
                    cekMakhluk = true;
                    while(cekKeterangan == false){
                        System.out.print("Pelanggan, Pegawai atau Pegawai Spesial: ");
                        keterangan = inp.nextLine().toLowerCase();
                        if(keterangan.equals("pelanggan") || keterangan.equals("pegawai") || keterangan.equals("pegawai spesial")){
                            System.out.print("Nama: ");
                            String nama = inp.nextLine();
                            System.out.print("Uang: ");
                            double uang = Double.parseDouble(inp.nextLine());

                            if(keterangan.equals("pelanggan")){
                                Manusia data = new Pelanggan(nama, uang);
                                objects.add(data);
                            }else if(keterangan.equals("pegawai spesial")){
                                System.out.print("Level keanggotaan: ");
                                String levelKeanggotaan = inp.nextLine();
                                Manusia data = new PegawaiSpesial(nama, uang, levelKeanggotaan);
                                objects.add(data);
                            }else{
                                System.out.print("Level keanggotaan: ");
                                String levelKeanggotaan = inp.nextLine();
                                Manusia data = new Pegawai(nama, uang, levelKeanggotaan);
                                objects.add(data);
                            }
                            cekKeterangan = true;
                        }else{
                            System.out.println("Maaf, Keterangan hanya antara pelanggan, pegawai atau pegawai spesial!");
                        }
                    }
                }else if(status.equals("hewan")){
                    cekMakhluk = true;
                    while(cekKeterangan == false){
                        System.out.println("Ikan, Ikan Spesial atau Burung Hantu: ");
                        keterangan = inp.nextLine().toLowerCase();
                        if(keterangan.equals("ikan") || keterangan.equals("burung hantu") || keterangan.equals("ikan spesial")){
                            System.out.print("Nama: ");
                            String nama = inp.nextLine();
                            System.out.print("Spesies: ");
                            String spesies = inp.nextLine();
                            if(keterangan.equals("burung hantu")){
                                Hewan data = new BurungHantu(nama, spesies);
                                objects.add(data);
                            }else if(keterangan.equals("ikan spesial")){
                                System.out.print("Apakah beracun(y/t)?: ");
                                String racun = inp.nextLine().toLowerCase();
                                if(racun.substring(0,1).equals("y") || racun.equals("iya")){
                                    booleanRacun = true;
                                }
                                Hewan data = new IkanSpesial(nama, spesies, booleanRacun);
                                objects.add(data);
                            }else{
                                System.out.print("Apakah beracun(y/t)?: ");
                                String racun = inp.nextLine().toLowerCase();
                                if(racun.substring(0,1).equals("y") || racun.equals("iya")){
                                    booleanRacun = true;
                                }
                                Hewan data = new Ikan(nama, spesies, booleanRacun);
                                objects.add(data);
                            }
                            cekKeterangan = true;
                        }else{
                            System.out.println("Maaf, Keterangan hanya antara Ikan, Ikan Spesial atau Burung Hantu!");
                        }
                    }
                }else{
                    System.out.println("Maaf, pilihan makhluk hanya hewan dan manusia");
                }
            }
            cekMakhluk = false;
            cekKeterangan = false;
        }

        String perintah = "";
        while(perintah.equals("selesai") == false){
            System.out.println("Silakan masukkan perintah: ");
            perintah = inp.nextLine().toLowerCase();
            for (int i = 0; i < objects.size(); i++) {
                try{
                    if(perintah.substring(0,((Manusia)objects.get(i)).getNama().length()).equals((((Manusia)objects.get(i)).getNama()).toLowerCase()) && perintah.substring(((Manusia)objects.get(i)).getNama().length(),((Manusia)objects.get(i)).getNama().length()+1).equals(" ") && perintah.split(" ")[perintah.split(" ").length-1].equals(perintah.substring(((Manusia)objects.get(i)).getNama().length()+1))){
                        indexObjek =i;
                        Perintah = perintah.split(" ")[perintah.split(" ").length-1];
                        break;
                    }
                }catch(ClassCastException e){
                    if(perintah.substring(0,((Hewan)objects.get(i)).getNama().length()).equals((((Hewan)objects.get(i)).getNama()).toLowerCase()) && perintah.substring(((Hewan)objects.get(i)).getNama().length(),((Hewan)objects.get(i)).getNama().length()+1).equals(" ") && perintah.split(" ")[perintah.split(" ").length-1].equals(perintah.substring(((Hewan)objects.get(i)).getNama().length()+1))){
                        indexObjek =i;
                        Perintah = perintah.split(" ")[perintah.split(" ").length-1];
                        break;
                    }
                }
            }
            
            if(perintah.equals("selesai")){
                System.out.println("Sampai jumpa!");
                inp.close();
            }else if(indexObjek == -1 && perintah.startsWith("panggil ")){
                for (int i = 0; i < objects.size(); i++) {
                    try{
                        if(perintah.substring(8).equals((((Manusia)objects.get(i)).getNama()).toLowerCase())){
                            indexObjek =i;
                            break;
                        }
                    }catch(ClassCastException e){
                        if(perintah.substring(8).equals((((Hewan)objects.get(i)).getNama()).toLowerCase())){
                            indexObjek =i;
                            break;
                        }
                    }
                }
                if(objects.get(indexObjek) instanceof Manusia){
                    System.out.println(((Manusia)objects.get(indexObjek)).bicara());
                }else{
                    System.out.println(((Hewan)objects.get(indexObjek)).bersuara());
                }
            }else if(perintah.equals("skip")){
                System.out.println("Maaf, perintah tidak ditemukan!");
            }else if(indexObjek == -1){
                for(int i = 0; i<perintah.split(" ").length-1; i++){
                    nama_objek += perintah.split(" ")[i]+" ";
                }
                System.out.println("Maaf, tidak ada makhluk bernama "+nama_objek);
                nama_objek = "";
            }else if(Perintah.equals("bergerak")){
                System.out.println(((Makhluk)objects.get(indexObjek)).bergerak());
            }else if(Perintah.equals("bernafas")){
                System.out.println(((Makhluk)objects.get(indexObjek)).bernafas());
            }else if(Perintah.equals("bicara")){
                if(objects.get(indexObjek) instanceof Manusia){
                    System.out.println(((Manusia)objects.get(indexObjek)).bicara());
                }else if (objects.get(indexObjek) instanceof Ikan || objects.get(indexObjek) instanceof IkanSpesial){
                    System.out.println("Maaf, Ikan "+((Hewan)objects.get(indexObjek)).getSpecies()+" tidak bisa bicara.");
                }else{
                    System.out.println("Maaf, Burung Hantu "+((Hewan)objects.get(indexObjek)).getSpecies()+" tidak bisa bicara.");
                }
            }else if(Perintah.equals("bekerja")){
                if(objects.get(indexObjek) instanceof Pegawai){
                    System.out.println(((Pegawai)objects.get(indexObjek)).bekerja());
                }else if(objects.get(indexObjek) instanceof PegawaiSpesial){
                    System.out.println(((PegawaiSpesial)objects.get(indexObjek)).bekerja());
                }else if (objects.get(indexObjek) instanceof Ikan || objects.get(indexObjek) instanceof IkanSpesial){
                    System.out.println("Maaf, Ikan "+((Hewan)objects.get(indexObjek)).getSpecies()+" tidak bisa bekerja.");
                }else if (objects.get(indexObjek) instanceof BurungHantu){
                    System.out.println("Maaf, Burung Hantu "+((Hewan)objects.get(indexObjek)).getSpecies()+" tidak bisa bekerja.");
                }else{
                    System.out.println("Maaf, Pelanggan "+((Manusia)objects.get(indexObjek)).getNama()+" tidak bisa bekerja");
                }
            }else if(Perintah.equals("libur")){
                if(objects.get(indexObjek) instanceof Pegawai){
                    System.out.println("Maaf, Pegawai "+((Manusia)objects.get(indexObjek)).getNama()+" tidak bisa libur");
                }else if(objects.get(indexObjek) instanceof PegawaiSpesial){
                    System.out.println(((PegawaiSpesial)objects.get(indexObjek)).libur());
                }else if (objects.get(indexObjek) instanceof Ikan || objects.get(indexObjek) instanceof IkanSpesial){
                    System.out.println("Maaf, Ikan "+((Hewan)objects.get(indexObjek)).getSpecies()+" tidak bisa libur.");
                }else if (objects.get(indexObjek) instanceof BurungHantu){
                    System.out.println("Maaf, Burung Hantu "+((Hewan)objects.get(indexObjek)).getSpecies()+" tidak bisa libur.");
                }else{
                    System.out.println("Maaf, Pelanggan "+((Manusia)objects.get(indexObjek)).getNama()+" tidak bisa libur");
                }
            }else if(Perintah.equals("terbang")){
                if(objects.get(indexObjek) instanceof Pelanggan){
                    System.out.println("Maaf, Pelanggan "+((Manusia)objects.get(indexObjek)).getNama()+" tidak bisa terbang");
                }else if (objects.get(indexObjek) instanceof Ikan){
                    System.out.println("Maaf, Ikan "+((Hewan)objects.get(indexObjek)).getSpecies()+" tidak bisa terbang.");
                }else if (objects.get(indexObjek) instanceof IkanSpesial){
                    System.out.println(((IkanSpesial)objects.get(indexObjek)).terbang());
                }else if (objects.get(indexObjek) instanceof BurungHantu){
                    System.out.println("Maaf, tidak ada perintah untuk Burung Hantu "+((Hewan)objects.get(indexObjek)).getSpecies()+" terbang.");
                }else{
                    System.out.println("Maaf, Pegawai "+((Manusia)objects.get(indexObjek)).getNama()+" tidak bisa terbang");
                }
            }else if(Perintah.equals("beli")){
                if(objects.get(indexObjek) instanceof Pelanggan){
                    System.out.println(((Pelanggan)objects.get(indexObjek)).beli());
                }else if (objects.get(indexObjek) instanceof Ikan || objects.get(indexObjek) instanceof IkanSpesial){
                    System.out.println("Maaf, Ikan "+((Hewan)objects.get(indexObjek)).getSpecies()+" tidak bisa beli.");
                }else if (objects.get(indexObjek) instanceof BurungHantu){
                    System.out.println("Maaf, Burung Hantu "+((Hewan)objects.get(indexObjek)).getSpecies()+" tidak bisa beli.");
                }else{
                    System.out.println("Maaf, Pegawai "+((Manusia)objects.get(indexObjek)).getNama()+" tidak bisa beli");
                }
            }else if(Perintah.equals("bersuara")){
                if(objects.get(indexObjek) instanceof Hewan){
                    System.out.println(((Hewan)objects.get(indexObjek)).bersuara());
                }else if (objects.get(indexObjek) instanceof Pelanggan){
                    System.out.println("Maaf, gunakan perintah \"bicara\" untuk mendengar suara pelanggan "+((Manusia)objects.get(indexObjek)).getNama());
                }else{
                    System.out.println("Maaf, gunakan perintah \"bicara\" untuk mendengar suara pegawai "+((Manusia)objects.get(indexObjek)).getNama());
                }
            }else{
                if(objects.get(indexObjek) instanceof Pelanggan){
                    System.out.println("Maaf, Pelanggan "+ ((Manusia)objects.get(indexObjek)).getNama() +" tidak bisa "+ Perintah);
                }else if (objects.get(indexObjek) instanceof Ikan || objects.get(indexObjek) instanceof IkanSpesial){
                    System.out.println("Maaf, Ikan "+ ((Hewan)objects.get(indexObjek)).getSpecies() +" tidak bisa "+ Perintah);
                }else if (objects.get(indexObjek) instanceof BurungHantu){
                    System.out.println("Maaf, Burung Hantu "+ ((Hewan)objects.get(indexObjek)).getSpecies() +" tidak bisa "+ Perintah);
                }else{
                    System.out.println("Maaf, Pegawai "+ ((Manusia)objects.get(indexObjek)).getNama() +" tidak bisa "+ Perintah);
                }
            }
            indexObjek = -1;
        }
    }
}
