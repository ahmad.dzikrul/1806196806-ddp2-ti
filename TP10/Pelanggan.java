public class Pelanggan extends Manusia implements Makhluk{

    public Pelanggan(String nama, double uang){
        setNama(nama);
        setUang(uang);
    }

    @Override
    public String bicara() {
      // TODO Auto-generated method stub
      return "Halo, saya "+ getNama() +". Uang saya adalah "+getUang()+".";
    }

    @Override
    public String bernafas() {
      // TODO Auto-generated method stub
      return getNama()+" bernafas dengan paru-paru.";
    }

    @Override
    public String bergerak() {
      // TODO Auto-generated method stub
      return getNama()+" bergerak dengan cara berjalan.";
    }

    public String beli(){
      return getNama()+" membeli makanan dan minuman di kedai VoidMain.";
  }
}
